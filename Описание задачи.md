# Цель работы

1. Настроить коммуникацию с разработчиком.
2. Оценить квалификацию и понимание задачи.
3. Оценить код, структуру проекта, культуру разработки.
4. Понять скорость работы.

# Описание

Программа предназначена для много пользовательского изменения значения счетчика.

## Надпись

Серверный компонент. 

Статическая надпись.

Текст надписи: “Я счетчик, хитрый!”.

## Счетчик

Клиентский компонент.

Содержит три элемента:

1. Кнопка -
2. Кнопка +
3. Надпись с текущем значением счетчика

## Требования

1. При нажатии кнопки - и + значение счётчика должно уменьшатся и увеличиваться.
2. Значение счётчика не может быть ниже нуля.
3. Если значение счётчика достигло нуля кнопка - должна стать *неактивной*.
4. Если значение счётчика достигло 5 тогда к значению счётчика нужно добавить 3.
5. Изменения счётчика должны быть видны одновременно всем клиентам.
6. Изменения счётчика должны отображаться без обновления страницы.
7. Изменения счётчика должны доставляться клиентам с минимальными задержками.
8. Изменения счётчика должны сохранятся в базу данных на сервере.
9. Изменения счётчика должны сохранятся до того как клиенты получат новое значение.
10. Должно быть исключено ошибочное изменение счётчика при задержке ответа сервера.

# Стек технологий

1. TypeScript.
2. DenoJS.
3. PostgreSQL.
4. React Native.
5. GraphQL.
6. Knex.js.
7. Next.js
8. SSR.
9. React Server Components. 
10. Веб-версия.
11. Мобильное приложение под Android.

# Порядок работ

1. **Подготовка**
    1. Создать репозиторий на **GitLab**.
    2. Создать пустой проект.
    3. Создать в корне проекта пустой файл “Описание задачи.md”.
    4. Скопировать текст этой задачи в файл “Описание задачи.md”.
    5. Отформатировать текст в **Markdown** для сохранения структуры оригинала.
    6. Создать в корне проекта пустой файл “Коннект.md”.
    7. Опубликовать проект в репозиторий.
    8. Отправить приглашение в репозиторий на аккаунт **@kurkovavArnold**, роль **Owner.**
2. **Разработка**
    1. Задать уточняющие вопросы.
    2. Обсудить новые зависимости.
    3. Разработать приложение.
    4. Делать коммиты, по чаще.
    5. Написать о том что разработка завершена.
3. **Завершение**
    1. Опубликовать тестовый сервер. 
    2. Опубликовать веб-версию и собрать apk.

# Прочее

1. Вопросы и уточнения приветствуется.
2. Для api нужно использовать только GraphQL.
3. В проекте нужно предусмотреть развертку с помощью Docker.
4. Все новые зависимости нужно сначала обсудить, пишите мне.
5. При планировании структуры приложения учтите что мы будем его расширять.
6. Мне важно видеть начальный коммит именно в тот момент когда вы начали работать.

