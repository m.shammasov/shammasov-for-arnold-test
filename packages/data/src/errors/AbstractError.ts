export default abstract class
        AbstractError<T extends string = 'AbstractError'> {

    public readonly isException = true

    protected constructor(public message: string, public readonly type: T) {

    }

}

export const isException = <O>(obj: O | AbstractError): obj is AbstractError => {
    // @ts-ignore
    if(obj['isException'])
        return true
    return false
}
