import AbstractError from "./AbstractError";


export default <T extends string>(type: T, message: string = 'Generic error of type '+ type) => {
    return class GenericError extends AbstractError<T> {
        public static readonly isTypeOf = <O, AbstractError>(obj: O | GenericError): obj is GenericError =>
            // @ts-ignore
            obj.type === type

        public static readonly type: T = type
        public constructor(message:string) {
            super(message, type)
        }
    }
}

type Clazz<T> = {
    new(lat: number, lng: number): T;
}

export type InstanceByClass<C> = C extends Clazz<infer T> ? T : never