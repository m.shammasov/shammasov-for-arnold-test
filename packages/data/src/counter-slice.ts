import {createSlice} from "@reduxjs/toolkit"

type Counter = number

export type CounterValueResponse = {
    counter: {
        value: number
    }
}

export type IncrementValueResponse = {
    increment: {
        value: number
    }
}

export type DecrementValueResponse = {
    decrement: {
        value: number
    }
}

export type CounterAction = Parameters<typeof counterSlice.reducer>[1]

export const INITIAL_COUNTER_VALUE = 0 as Counter

const COUNTER_MARK = 5
const COUNTER_JUMP = 3


const counterSlice = createSlice({
        name: 'Counter',
        initialState: INITIAL_COUNTER_VALUE,
        reducers: {
            increment: (state) => {
                return state === COUNTER_MARK - 1
                    ? state + COUNTER_JUMP + 1
                    : state + 1
            },
            // TODO Reducer analyzed by the arguments length, need to be a regular function
            decrement: (state) => {
                return Math.max(0, state - 1)
            },
        },
    })

export default counterSlice
