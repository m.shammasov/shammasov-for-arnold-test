/*
  Warnings:

  - You are about to drop the `Counter` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `ImmutableEvent` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "Counter";

-- DropTable
DROP TABLE "ImmutableEvent";

-- CreateTable
CREATE TABLE "Counter" (
    "value" INTEGER NOT NULL DEFAULT 0
);

-- CreateIndex
CREATE UNIQUE INDEX "Counter_value_key" ON "Counter"("value");
