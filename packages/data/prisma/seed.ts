import {INITIAL_COUNTER_VALUE} from "../src/counter-slice";
import getPrisma from "./get-prisma";


async function main() {
  await getPrisma().counter.deleteMany()
  await getPrisma().counter.create({
          data:{value: INITIAL_COUNTER_VALUE}
      },
  )
  console.info('Counter initialized with start value '+ INITIAL_COUNTER_VALUE)
}

main()
  .then(async () => {
    await getPrisma().$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await getPrisma().$disconnect()
    process.exit(1)
  })