import {PrismaClient,} from "@prisma/client"
import {CounterAction, INITIAL_COUNTER_VALUE} from "../src/counter-slice";
import {Prisma} from "@prisma/client/scripts/default-index";
import {counterSlice} from "../src";
import CounterCommandNotAppliedError from "../src/errors/CounterCommandNotAppliedError";

const prismaClient: PrismaClient = new PrismaClient()

const counterTable = prismaClient.counter

export type PrismaDB = typeof prisma

const createCounter  = async (value = INITIAL_COUNTER_VALUE) => {
    await counterTable.deleteMany()
    return (
        await counterTable.create({
            data:{value},
            select:{value: true}}
        )).value
}

const  selectCounter= async() => {
    const row = (await counterTable.findFirst({ select:{value:true}}))
    return row ? row.value : await createCounter()
}

const updateCounter = async (value: number) => {
    await counterTable.deleteMany()
    return (await createCounter(value))
}


export const prisma = prismaClient.$extends({
    model: {
        counter: {
            selectCounter,
            createCounter,
            updateCounter,
            async handleCounterCommand(event: CounterAction)  {
                const currentValue = await selectCounter()
                const nextValue = counterSlice.reducer(currentValue, event)

                if(currentValue === nextValue)
                    return new CounterCommandNotAppliedError(`Counter Command ${event.type} is not applied ${currentValue}`)

                await updateCounter(nextValue)
                return {value:nextValue}
            }

        }
    }
})
export default () => prisma