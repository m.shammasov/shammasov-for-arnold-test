// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import {createYoga} from 'graphql-yoga'
import type {NextApiRequest, NextApiResponse} from 'next'
import {schema} from './schema'
import prismaContext from "./prisma-context";
import { useGraphQLSSE } from '@graphql-yoga/plugin-graphql-sse'
 


export default createYoga<{
  req: NextApiRequest
  res: NextApiResponse
}>({
  /*graphiql: {
    // Use graphql-sse in GraphiQL.
    subscriptionsProtocol: 'SSE',
  },
  plugins: [
    // Simply install the graphql-sse plugin and you're off!
    useGraphQLSSE()
  ],*/
  graphqlEndpoint: '/api/yoga/graphql',
  schema,
  context:prismaContext,
})
