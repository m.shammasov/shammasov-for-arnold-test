import getPrisma from "@local/data/prisma/get-prisma"

export type PrismaContext = typeof prismaContext

const prismaContext = {
    prisma: getPrisma()
}

export default prismaContext