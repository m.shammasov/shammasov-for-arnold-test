import {makeSchema,} from 'nexus'
import * as Path from 'path'
import {
  MutationCounterDef,
  MutationCounterResult,
  QueryCounterDef,
  MutationCounterException,
  MutationCounterSuccess,
  SubscriptionCounterDef, CounterResult,
} from "./graphql-types/counter-defs";

export const schema = makeSchema({
  types:{MutationCounterDef, MutationCounterResult,CounterResult,QueryCounterDef,MutationCounterException,MutationCounterSuccess,SubscriptionCounterDef},//ObjectCounterDef],//QueryCounterDef,MutationCounterDef,],
  sourceTypes: {
    modules: [{ module: '.prisma/client', alias: 'PrismaClient' }],
  },
  contextType: {
    module: Path.join(process.cwd(), 'pages','api','yoga', 'prisma-context.ts'),
    export: 'PrismaContext',
    alias: 'PrismaContext'
  },
  outputs: {
    typegen: Path.join(
        __dirname,
        '..',
        '..',
        '..',
        '..',
        '..',
        'node_modules',
        '.nexus-prisma',
        'index.d.ts',
    ),
    schema: Path.join(process.cwd(), 'api.graphql'),
  },
})

export default schema