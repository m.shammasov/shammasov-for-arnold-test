import {extendType,objectType,unionType} from 'nexus'
import {PrismaClient} from "@prisma/client";
import {counterSlice} from "@local/data";
import getPrisma, {PrismaDB} from "@local/data/prisma/get-prisma";
import CounterCommandNotAppliedError from "@local/data/src/errors/CounterCommandNotAppliedError";
import {createPubSub} from 'graphql-yoga'
import {isException} from "@local/data/src/errors/AbstractError";

export const MutationCounterException = objectType({
    name: 'MutationCounterException',//CounterCommandNotAppliedError.type,
    definition(t) {
        t.string('type')
        t.string('message')
    }

})
export const MutationCounterSuccess = objectType({
    name: 'MutationCounterSuccess',
    definition(t) {
        t.boolean('isException')
        t.int('value')
    }
})

export const CounterResult = objectType({
    name: 'CounterResult',
    definition(t) {
        t.int('value')
    }
})

export const MutationCounterResult = unionType({
    name: 'MutationCounterResult',
    resolveType: (data)=> {
        const __typename = isException(data) ? 'MutationCounterException' : 'MutationCounterSuccess'
        if (!__typename) {
            throw new Error(`Could not resolve the type of data passed to union type "SearchResult"`)
        }
        return __typename
    },
    definition(t) {
        t.members('MutationCounterException', 'MutationCounterSuccess')
    }
})

export const QueryCounterDef = extendType({
  type: 'Query',
  definition: t =>
    t.field('counter', {
        type:'CounterResult',
      resolve: async (_, __, ctx) =>
          ({value: await (ctx.prisma as PrismaDB).counter.selectCounter()})
    })
})



export const MutationCounterDef = extendType({
    type: 'Mutation',
    definition(t) {
      t.field('increment', {
        type: 'MutationCounterResult',
        resolve: async  (_root, args, ctx) => {
            const result = await (ctx.prisma as PrismaDB).counter
                .handleCounterCommand(counterSlice.actions.increment())

            if(!isException(result))
                pubSub.publish('counter', result)

            return result
        }
      })
      t.field('decrement', {
        type: 'MutationCounterResult',

        resolve: async (_root, args, ctx)  => {
            const result = await (ctx.prisma as PrismaDB).counter
                .handleCounterCommand(counterSlice.actions.decrement())

            if(!isException(result))
                pubSub.publish('counter', result)

            return result
        }
      })
    }
})

const pubSub = createPubSub()

export const SubscriptionCounterDef = extendType({
  type: 'Subscription',
  definition(t) {
    t.field('counter',{
        type:'MutationCounterSuccess',

            subscribe: () => pubSub.subscribe('counter'),

        resolve(eventData) {
            return eventData
        },
    })
  }
})