import React from 'react'
import { GraphQLClient, ClientContext } from 'graphql-hooks'
import {UIRoot} from "./UIRoot";
import { createClient } from 'graphql-sse'
import {GRAPHQL_QUERIES_PATH, GRAPHQL_SUBSCRIPTIONS_PATH} from "@local/data/src/constants";
const client = new GraphQLClient({
    url: GRAPHQL_QUERIES_PATH,
    subscriptionClient: () =>
        createClient({

            url: GRAPHQL_SUBSCRIPTIONS_PATH
            /* additional config options */
        })
})


export const App = () => {
    return (
        <ClientContext.Provider value={client}>
            <UIRoot/>
        </ClientContext.Provider>
    )
}