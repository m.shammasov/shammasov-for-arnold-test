import {useReducer} from "react";
import {counterSlice} from "@local/data"
import {INITIAL_COUNTER_VALUE} from "@local/data/src/counter-slice";

export default () => {

    const [value, dispatch] = useReducer(counterSlice.reducer, INITIAL_COUNTER_VALUE);

    const onIncrement = () =>
        dispatch(counterSlice.actions.increment())

    const onDecrement = () =>
        dispatch(counterSlice.actions.decrement())

    return {value, onIncrement, onDecrement}

}
