import {useReducer, useState} from "react";
import {counterSlice} from "@local/data"
import {
    CounterValueResponse,
    DecrementValueResponse,
    IncrementValueResponse,
    INITIAL_COUNTER_VALUE
} from "@local/data/src/counter-slice";
import {useManualQuery, useQuery} from "graphql-hooks";
import CounterCommandNotAppliedError from "@local/data/src/errors/CounterCommandNotAppliedError";
import {InstanceByClass} from "@local/data/src/errors/exceptionFactory";
import {isException} from "@local/data/src/errors/AbstractError";
import {useMotion, useMount} from "react-use";


export const QUERY_COUNTER_VALUE = `
query QueryCounterState {
  counter{
  
    value
  }
}
`
export const INCREMENT_COUNTER_MUTATION = `
mutation Increment {
  increment{
     __typename
    ... on  MutationCounterException{
      type
      message
    }
    ... on  MutationCounterSuccess{
      value
    }
  }
}
`

export const DECREMENT_COUNTER_MUTATION = `
mutation Decrement {
  decrement{
     __typename
    ... on  MutationCounterException{
      type
      message
    }
    ... on  MutationCounterSuccess{
      value
    }
  }
}
`

export default () => {

    const [value,setValue] = useState(null as number | undefined | null)

    const [queryCounter, valueQuery] = useManualQuery<CounterValueResponse>(QUERY_COUNTER_VALUE)

    useMount( async ( ) => {
        setValue((await queryCounter()).data?.counter.value)
    })


    console.log('valueQuery', valueQuery)
    const [pushIncrement, incrementQuery] = useManualQuery<IncrementValueResponse | InstanceByClass<typeof CounterCommandNotAppliedError>>
            (INCREMENT_COUNTER_MUTATION)

    const [pushDecrement, decrementQuery] = useManualQuery<DecrementValueResponse | InstanceByClass<typeof CounterCommandNotAppliedError>>
            (DECREMENT_COUNTER_MUTATION)

    const onIncrement = async () => {
        const result  = await pushIncrement()
        console.log('Increment result', result)
        if(isException(result))
            console.warn('Exception ', result)
        else
            setValue(result.data?.increment.value)
    }

    const onDecrement = async () => {
        const result  = await pushDecrement()
        console.log('Decrement result', result)
        if(isException(result)) {
            setValue(undefined)
            console.warn('Exception ', result)
        }
        else
            setValue(result.data?.decrement.value)
    }
    return {value , onIncrement, onDecrement}

}
