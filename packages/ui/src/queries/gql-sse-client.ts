import { createClient } from 'graphql-sse'

const client = createClient({
    // singleConnection: true, use "single connection mode" instead of the default "distinct connection mode"
    url: '/api/yoga/graphql'
})