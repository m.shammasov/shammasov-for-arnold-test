import React, {useState} from "react";
import {SafeAreaView, StyleSheet, Text,} from "react-native";
import Counter from "./components/CounterView";
import useLocalCounter from "./hooks/use-local-counter";
import ServerSideTitle from "./components/ServerSideTitle";
import {GraphQLClient, useSubscription} from "graphql-hooks";
import {GRAPHQL_SUBSCRIPTIONS_PATH} from "@local/data/src/constants";
import useRemoteCounter from "./hooks/use-remote-counter";
export const COUNTER_VALUE_SUBSCRINTION = `
subscription L{
    counter{
        value
    }
}
`


const client = new GraphQLClient({
    url: 'http://localhost:8000/graphql',
    /*subscriptionClient: () =>
      new SubscriptionClient('ws://localhost:3000'+GRAPHQL_SUBSCRIPTIONS_PATH, {
        /* additional config options */
      //})
    // or
   /* subscriptionClient: () =>
      createClient({
        url: 'ws://localhost:8000/graphql'
        /* additional config options */
      //})
  })

export function UIRoot() {
    const [count, setCount] = useState(0)
    const [error, setError] = useState(null)
    const counterApi = useRemoteCounter()
    useSubscription({ query: COUNTER_VALUE_SUBSCRINTION }, (result) => {
        const  { data, errors } =result 
        console.log(result)
      /*  if (errors && errors.length > 0) {
            // handle your errors
            setError(errors[0])
            return
        }

        // all good, handle the gql result
        setCount(data.value)*/
    })
    return (
        <SafeAreaView style={styles.root}>
            {/* On React Native for Web builds coming from CRA, TypeScript
              complains about the image type, so we cast it as a workaround  */}

            <ServerSideTitle/>

            <Counter {...counterApi}/>


        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    root: {
        paddingTop:50,
        height: "100%",

        alignItems: 'center',

        
        backgroundColor: "white",
    },
})