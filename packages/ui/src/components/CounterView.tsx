import React from "react";
import {Button, StyleSheet, Text, View,} from "react-native";



export type CounterProps = {
    value: number
    onIncrement: () => any
    onDecrement: () => any 
}

export default ({onDecrement,onIncrement,value}:CounterProps) => {
  return (
      <View style={styles.counterContainer}>
          <View style={styles.buttonContainer}>
              <Button title="-" disabled={value === 0} onPress={onDecrement} />
          </View>
          <View style={styles.valueContainer}>
              <Text style={styles.valueText}>{
                  value === undefined
                    ? 'ошибка'
                    : value === null
                        ? ''
                        : value
                }
              </Text>
          </View>
          <View style={styles.buttonContainer} >
              <Button title="+" onPress={onIncrement} />
          </View>
      </View>

  
  );
}

const styles = StyleSheet.create({
    counterContainer: {
        width:400,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    valueContainer: {  alignItems:"center", flex: 2},
    buttonContainer: {
        flex: 0.4,
    },

    valueText: {
        fontFamily: "roboto-700",
        color: "rgba(31,10,207,1)",
        height: 33,
        width: 118,
        textAlign: "center",
        lineHeight: 24,
        fontSize: 32
    }
});